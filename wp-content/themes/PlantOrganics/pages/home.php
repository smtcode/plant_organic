<?php

/**
 * Template Name: Home
 *
 * @package PlantOrganics
 * @author  PlantOrganics
 * @license GPL-2.0+
 * @link    http://www.PlantOrganics.com/
 */

remove_action( 'genesis_loop', 'genesis_do_loop' );
add_action( 'genesis_after_header', 'home' );

 /** Code for custom loop */
function home() {
    echo '<div class="site-inner">';
    if ( have_posts() ) : while ( have_posts() ) : the_post();   
        $home_intro = get_field('home_intro');
        $section_banner = get_field('section_banner');
        $background_image_banner = get_field('background_image_banner');
        $crafting_section = get_field('crafting_section');	
        $home_intro['intro_title'] ? $home_intro['intro_title'] : '' ;
        $home_intro['tag_title'] ? $home_intro['tag_title'] : '' ;
        $home_intro['image_intro'] ? $home_intro['image_intro'] : '' ;
        $home_intro['add_feature'] ? $home_intro['add_feature'] : '' ;
        if ($home_intro['image_intro']=='') {
        }
        $crafting_section['title_about'] ? $crafting_section['title_about'] : '' ;
        $crafting_section['image_craft'] ? $crafting_section['image_craft'] : '' ;
        $crafting_section['content_craft'] ? $crafting_section['content_craft'] : '' ;
        $crafting_section['link_craft'] ? $crafting_section['link_craft'] : '' ;
        $crafting_section['link_craft']['title'] ?$crafting_section['link_craft']['title'] : '' ;
       
        ?>
        <section class="main-banner" style="background-image:url(<?php echo $background_image_banner ?>);">
            <div class="container">
                <h1><?php echo $section_banner ?></h1>
            </div>
        </section>
        <section class="intro-section" data-aos="fade-zoom-in" data-aos-duration="800" data-aos-easing="ease-in-sine" data-aos-delay="800" data-aos-offset="0" data-aos-once="true">
            <div class="wrap">
                <div class="row">
                    <div class="col-md-7 content">
                         <h2 id="typer"><?php echo $home_intro['intro_title']; ?><span><?php echo $home_intro['tag_title']; ?></span></h2>
                        <p><?php echo $home_intro['intro_content']; ?></p>
                        <?php

                        if( have_rows('home_intro') ): while ( have_rows('home_intro') ) : the_row(); ?>
                                <?php
                                if( have_rows('add_feature') ): ?>
                                <div class="circles-wrap">
                                <?php
                                    while ( have_rows('add_feature') ) : the_row();       
                                // vars
                                $title_feature = get_sub_field('title_feature');
                                $background_color = get_sub_field('background_color'); ?>
                                    <div class="circle-item" style="background-color: <?php echo  $background_color; ?>;"><span><?php echo $title_feature; ?></span></div>
                                <?php
                                endwhile; ?>
                                </div>
                                <?php
                                endif;?>
                            <?php
                        endwhile; endif;
                        ?>
                        <div class="features-wrap">
                         <?php
                          if( have_rows('home_intro') ): while ( have_rows('home_intro') ) : the_row(); 
                                if( have_rows('add_feature_icon') ): while ( have_rows('add_feature_icon') ) : the_row(); 
                                    // vars
                                    $title_feature = get_sub_field('title_feature');
                                    $icon = get_sub_field('icon'); ?>
                                        <div class="feature-item">
                                            <img src="<?php echo $icon;?>" alt="Icon">
                                            <span><?php echo  $title_feature; ?></span>
                                        </div>
                                    <?php
                                endwhile; endif;
                        endwhile; endif;
                        ?>
                         </div>
                    </div>
                    <div class="col-md-5 image">
                        <img src="<?php echo $home_intro['image_intro']; ?>">
                    </div>
                </div>
            </div>
        </section>
        <?php 
             $feature_products = get_field('feature_products');
        ?>
        <section class="products-related">
            <div class="wrap">
                <div class="row justify-content-between">
                <?php  
                    if( empty($feature_products)){ 
                        $args = array(
                            'post_type'      => 'product',
                            'posts_per_page' => 10,
                        );
    
                        $loop = new WP_Query( $args );
    
                        while ( $loop->have_posts() ) : $loop->the_post();
                            global $product;
                                ?>
                            <div class="col-md-4" data-aos="fade-up" data-aos-duration="500" data-aos-easing="ease-in-sine" data-aos-delay="800" data-aos-offset="0" data-aos-once="true">
                                <div class="products-select">
                                        <div class="img-home-p">
                                            <a href="<?php echo get_permalink( $p->ID ); ?>"><img src="<?php echo get_the_post_thumbnail_url(  $p->ID ) ?>" alt=""></a>
                                        </div>
                                        <h3 class="product-custom-tt">
                                            <a href="<?php echo get_permalink( $p->ID ); ?>"><?php echo get_the_title( $p->ID ); ?></a>
                                        </h3>
                                        <p class="maximun"><?php echo get_the_excerpt( $p->ID ); ?></p>
                                        <?php
                                            $price = get_post_meta( $p->ID , '_regular_price', true);
                                        ?>
                                        <p class="price"><?php echo $price; ?></p>
                                        <!-- <a href="<?php echo get_permalink( $p->ID ); ?>"><img src="<?php echo get_stylesheet_directory_uri() ?>/images/arrow.png" alt=""></i></a> -->
                                </div>
                            </div>
                        <?php
                        endwhile;
    
                        wp_reset_query();
                    }  
                ?>
                <?php if( $feature_products ): ?>
                <?php foreach( $feature_products as $p ):?>
                    <div class="col-md-4" data-aos="fade-up" data-aos-duration="500" data-aos-easing="ease-in-sine" data-aos-delay="800" data-aos-offset="0" data-aos-once="true">
                        <div class="products-select">
                                <div class="img-home-p">
                                    <a href="<?php echo get_permalink( $p->ID ); ?>"><img src="<?php echo get_the_post_thumbnail_url(  $p->ID ) ?>" alt=""></a>
                                </div>
                                <h3 class="product-custom-tt">
                                    <a href="<?php echo get_permalink( $p->ID ); ?>"><?php echo get_the_title( $p->ID ); ?></a>
                                </h3>
                                <p class="maximun"><?php echo get_the_excerpt( $p->ID ); ?></p>
                                <?php
                                    $price = get_post_meta( $p->ID , '_regular_price', true);
                                ?>
                                <p class="price"><?php echo $price; ?></p>
                                <!-- <a href="<?php echo get_permalink( $p->ID ); ?>"><img src="<?php echo get_stylesheet_directory_uri() ?>/images/arrow.png" alt=""></i></a> -->
                        </div>
                    </div>
                <?php endforeach; ?>
            <?php endif; ?>
                </div>
            </div>
        </section>

        <section class="section-crafting" style="background-image:url(<?php echo $crafting_section['image_craft']; ?>);">
            <div class="wrap" data-aos="fade-down" data-aos-duration="500" data-aos-easing="ease-in-sine" data-aos-delay="800" data-aos-offset="0" data-aos-once="true">
                <!-- <h2><?php echo $crafting_section['title']; ?></h2> -->
                <div class="row align-items-center crafting-content">
                    <div class="col-md-6"> 
                        <div class="content-text">
                            <h2><?php echo $crafting_section['title_about']; ?></h2>
                            <!-- <?php echo $crafting_section['content_craft']; ?> -->
                        </div>
                        <a href="<?php echo $crafting_section['link_craft']['url']; ?>"> <?php echo $crafting_section['link_craft']['title']; ?></a>
                    </div>
                    <div class="col-md-6 content-img">
                        <!-- <img src="<?php echo $crafting_section['image_craft']; ?>"> -->
                    </div>
                </div>
            </div>
        </section>
        <?php
            $title_faq = get_field('title_faq');
            $add_faq = get_field('add_faq');
            $more_question = get_field('more_question');
            
        ?>
        <section class="section-faqs">
            <div class="wrap">
                <h2><?php echo $title_faq; ?></h2>
                <?php if( have_rows('add_faq') ): ?>
                    <ul class="slides">
                    <?php while( have_rows('add_faq') ): the_row(); 
                        // vars
                        $question = get_sub_field('question');
                        $answer = get_sub_field('answer');
                        ?>
                       <div class="faq-wrap">
                            <h3 class="question"><?php echo $question ?></h3>
                            <div class="answer" style="display:none">
                                <p><?php echo $answer ?></p>
                            </div>
                        </div>
                    <?php endwhile; ?>
                    </ul>
                <?php endif; ?>
                <div class="more-questions">
                    <p class="more-questions"><?php echo $more_question ?></p>
                </div>
            </div>
        </section>
        <?php
    endwhile;
    endif;
    wp_reset_query();

    echo '</div>';

}
add_filter( 'genesis_markup_site-inner', '__return_null' );
 genesis();