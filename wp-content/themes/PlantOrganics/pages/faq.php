<?php

/**
 * Template Name: FAQ
 *
 * @package PlantOrganics
 * @author  PlantOrganics
 * @license GPL-2.0+
 * @link    http://www.PlantOrganics.com/
 */

remove_action( 'genesis_loop', 'genesis_do_loop' );
add_action( 'genesis_after_header', 'faq' );

 /** Code for custom loop */
function faq() {
    ?>
    <?php
        $title_faq = get_field('title_faq');
        $add_faq_section = get_field('add_faq_section');
        $more_question = get_field('more_question');
    ?>
        <section class="section-faqs">
            <div class="wrap">
                <h2><?php echo $title_faq; ?></h2>
                <?php if( have_rows('add_faq_section') ): ?>
                    <ul class="slides">
                    <?php while( have_rows('add_faq_section') ): the_row(); 
                        // vars
                        $question = get_sub_field('question');
                        $answer = get_sub_field('answer');
                        ?>
                       <div class="faq-wrap">
                            <h3 class="question"><?php echo $question ?></h3>
                            <div class="answer" style="display:none">
                                <p><?php echo $answer ?></p>
                            </div>
                        </div>
                    <?php endwhile; ?>
                    </ul>
                <?php endif; ?>
                <div class="more-questions">
                    <p class="more-questions"><?php echo $more_question ?></p>
                </div>
            </div>
        </section>
        <?php
    wp_reset_query();

}
add_filter( 'genesis_markup_site-inner', '__return_null' );
 genesis();