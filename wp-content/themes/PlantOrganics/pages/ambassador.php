<?php

/**
 * Template Name: Ambassador Program
 *
 * @package PlantOrganics
 * @author  PlantOrganics
 * @license GPL-2.0+
 * @link    http://www.PlantOrganics.com/
 */

remove_action( 'genesis_loop', 'genesis_do_loop' );
add_action( 'genesis_after_header', 'ambassador' );

 /** Code for custom loop */
function ambassador() {
    $image_ambassador = get_field( 'image_ambassador');
    $title_ambassador = get_field( 'title_ambassador');
    $sub_line = get_field( 'sub_line');
    $content_ambassador = get_field( 'content_ambassador');
    ?>
    <section class="ambassador-section">
        <div class="container">
            <div class="row row-ambassador mx-0">
                <div class="col-lg-6 ambassador-img">
                    <img src=" <?php echo $image_ambassador ?>" alt="">
                </div>
                <div class="col-lg-6 ambassador-content">
                    <h2><?php echo $title_ambassador ?> <span><?php echo $sub_line ?></span></h2>
                    <?php echo $content_ambassador ?> 
                    <?php $link_ambassador = get_field( 'link_ambassador' ); ?>
                    <?php if ( $link_ambassador ) { ?>
                        <div class="text-center mt-3">
                            <a href="<?php echo $link_ambassador['url']; ?>" class="btn btn-green" target="<?php echo $link_ambassador['target']; ?>"><?php echo $link_ambassador['title']; ?></a>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
        <div class="gray-spacer"></div>
    </section>
    <?php
}
add_filter( 'genesis_markup_site-inner', '__return_null' );
 genesis();